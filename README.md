# COREWAR

## Table of Content
- [Corewar](#corewar)
- [ASM](#asm)
- [Champion](#champion)
- [Visualizer](#visualizer)
- [Authors](#authors)

## Corewar
To work on Corewar VM:
```bash
$> git checkout -b vm
$> git pull origin vm
```

## ASM
To work on Corewar ASM:
```bash
$> git checkout -b asm
$> git pull origin asm
```


## Champion
Champion development will start when VM and ASM parts are ready

## Visualizer
Visualizer development will start when VM and ASM parts are ready


## Authors
edjubert <edjubert@student.42.fr>
fldoucet <fldoucet@student.le-101.fr>
maabou-h <maabou-h@student.42.fr>
sdincbud <sdincbud@student.42.fr>
