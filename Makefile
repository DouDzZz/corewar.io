# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/04 18:02:25 by edjubert          #+#    #+#              #
#    Updated: 2019/05/29 17:22:00 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

COREWAR			:=		corewar
ASM				:=		asm

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  DIRECTORIES                                 #

SRC_DIR_COREWAR	:=		./srcs
SRC_DIR_ASM		:=		./srcs
INC_DIR			:=		./includes
OBJ_DIR_COREWAR	:=		./obj
OBJ_DIR_ASM		:=		./obj
LIBFT_FOLDER	:=		./libft
LIBFT			:=		$(LIBFT_FOLDER)/libft.a
INC_LIB			:=		./libft/includes

#==============================================================================#
#------------------------------------------------------------------------------#
#                                   COREWAR                                    #

SRC_COREWAR		:=		corewar.c

OBJ_COREWAR		:=		$(addprefix $(OBJ_DIR_COREWAR)/,$(SRC_COREWAR:.c=.o))
NB_COREWAR		:=		$(words $(SRC))
INDEX_COREWAR	:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     ASM                                      #

SRC_ASM			:=		asm.c

OBJ_ASM			:=		$(addprefix $(OBJ_DIR_ASM)/,$(SRC_ASM:.c=.o))
NB_ASM			:=		$(words $(SRC_ASM))
INDEX_ASM		:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                               COMPILER & FLAGS                               #

GCC				:=		gcc
FLAGS			:=		-Wall					\
						-Wextra					\
						-Werror					\
						-g

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     RULES                                    #

all:					$(COREWAR) $(ASM)

$(COREWAR):				$(OBJ_COREWAR)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) \
		--no-print-directory; fi
	@ $(GCC) $(FLAGS) $(OBJ_COREWAR) $(LIBFT) -o $(COREWAR)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' \
		"Compilation of " $(COREWAR) " is done ---"

$(ASM):				$(OBJ_ASM)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) \
		--no-print-directory; fi
	@ $(GCC) $(FLAGS) $(OBJ_ASM) $(LIBFT) -o $(ASM)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' \
		"Compilation of " $(ASM) " is done ---"

$(OBJ_DIR_COREWAR)/%.o:	$(SRC_DIR_COREWAR)/%.c
	@ mkdir -p $(OBJ_DIR_COREWAR)
	@ $(eval DONE = $(shell echo $$(($(INDEX_COREWAR) * 20 / $(NB_COREWAR)))))
	@ $(eval PERCENT = $(shell \
		echo $$(($(INDEX_COREWAR) * 100 / $(NB_COREWAR)))))
	@ $(eval TO_DO = $(shell echo "$@"))
	@ $(GCC) $(FLAGS) -I$(INC_LIB) -I$(INC_DIR) -c $< -o $@
	@ printf "                                                          \r"
	@ printf "\033[33m[ %3d%% ] %s\t%s\r\033[0m" $(PERCENT) $(COREWAR) $@
	@ $(eval INDEX = $(shell echo $$(($(INDEX_COREWAR) + 1))))

$(OBJ_DIR_ASM)/%.o:			$(SRC_DIR_ASM)/%.c
	@ mkdir -p $(OBJ_DIR_ASM)
	@ $(eval DONE = $(shell echo $$(($(INDEX_ASM) * 20 / $(NB_ASM)))))
	@ $(eval PERCENT = $(shell echo $$(($(INDEX_ASM) * 100 / $(NB_ASM)))))
	@ $(eval TO_DO = $(shell echo "$@"))
	@ $(GCC) $(FLAGS) -I$(INC_LIB) -I$(INC_DIR) -c $< -o $@
	@ printf "                                                          \r"
	@ printf "\033[33m[ %3d%% ] %s\t%s\r\033[0m" $(PERCENT) $(ASM) $@
	@ $(eval INDEX = $(shell echo $$(($(INDEX_ASM) + 1))))

clean:
	@ /bin/rm -rf $(OBJ_DIR_COREWAR) $(OBJ_DIR_ASM)
	@ make -C $(LIBFT_FOLDER) clean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' \
		"CLEAN  of " $(COREWAR) " is done ---"
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' \
		"CLEAN  of " $(ASM) " is done ---"

fclean:				clean
	@ /bin/rm -rf $(COREWAR) $(ASM)
	@ make -C $(LIBFT_FOLDER) fclean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' \
		"FCLEAN of " $(COREWAR) " is done ---"
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' \
		"FCLEAN of " $(ASM) " is done ---"

re:				fclean all

.PHONY: all clean fclean re
